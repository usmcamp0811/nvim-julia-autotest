# About 

This is a simple plugin that will automatically run your Julia tests when you save your `runtest.jl` file. 
It will then display the results in the buffer as virtual text. This is still very much a work in progress.


## Demo 

![Demo](./screenshot.png)


## Install with Packer

Requires `nvim >= 0.8`

```lua 
use({"https://gitlab.com/usmcamp0811/nvim-julia-autotest",
  config = function()
    require("julia-autotest").setup()
  end
})
```


